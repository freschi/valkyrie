package at.femoweb.secnet.server;

import at.femoweb.secnet.API;
import at.femoweb.secnet.CertificateStore;
import at.femoweb.secnet.NetworkCertificate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.security.SecureRandom;
import java.util.Properties;
import java.util.Scanner;

/**
 * Created by felix on 12/31/14.
 */
public class Startup {

    private static final Logger log = LogManager.getLogger("Secnet-Server");

    public static void main(String[] args) {
        log.info("Starting Server with Version " + Server.VERSION);
        log.debug("Checking API compatibility level");
        log.debug("Linked API has API level " + API.API_LEVEL);
        log.debug("This Version requires API level " + Server.REQUIRED_API_LEVEL);
        if(API.API_LEVEL == Server.REQUIRED_API_LEVEL) {
            log.info("API level matches required level");
        } else if (API.API_LEVEL > Server.REQUIRED_API_LEVEL) {
            log.warn("API level is higher than required API level. Compatibility issues may occur");
        } else if (API.API_LEVEL < Server.REQUIRED_API_LEVEL) {
            log.fatal("API Level mismatch!");
            log.fatal("Consider upgrading the secnet-core.jar in your classpath");
            log.fatal("Exiting server!");
            System.exit(1);
        }
        log.info("Loading Components");
        log.debug("Setting certificates store password requester");
        CertificateStore.setPasswordRequest(request -> {
            System.out.println("Please enter Password for '" + request + "'");
            Scanner scanner = new Scanner(System.in);
            return scanner.next();
        });
        log.info("Loading Certificate store");
        try {
            CertificateStore.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Properties properties = new Properties();
        log.debug("Loading config");
        File file = new File("conf/server.conf");
        if(!file.exists()) {
            log.warn("No configuration found");
            log.info("Creating it");
            properties.setProperty("network.name", "Testnet");
            properties.setProperty("network.admin", "__keymaster");
            log.debug("Storing configuration");
            try {
                file.getParentFile().mkdirs();
                properties.store(new FileOutputStream(file), "Secnet Server configuration");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                properties.load(new FileInputStream(file));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(!CertificateStore.has("net-" + properties.getProperty("network.name"))) {
            log.debug("Creating network certificate");
            NetworkCertificate networkCertificate = new CertificateGenerator().generateNetworkCertificate(new SecureRandom(), properties.getProperty("network.name"), properties.getProperty("network.admin"));
            CertificateStore.add(networkCertificate);
            try {
                CertificateStore.store();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            NetworkCertificate certificate = (NetworkCertificate) CertificateStore.get("net-" + properties.getProperty("network.name"));
            log.info("Bootstrapping network " + certificate.getNetworkName());
            log.info("Network Admin "  + certificate.getNetworkAdmin());
            log.info("Network Key length " + certificate.getNetworkKey().length);
            log.info("Registered Network Hosts " + certificate.getNetworkHosts().size());
            for(String host : certificate.getNetworkHosts()) {
                log.debug("Registered Host " + host);
            }
        }
    }
}
