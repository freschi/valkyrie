package at.femoweb.secnet.server;

import at.femoweb.secnet.NetworkCertificate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.Enumeration;

/**
 * Created by felix on 1/1/15.
 */
public class CertificateGenerator {

    private static final Logger log = LogManager.getLogger("Secnet-Server");

    public NetworkCertificate generateNetworkCertificate(SecureRandom random, String networkName, String networkAdmin) {
        NetworkCertificate networkCertificate = new NetworkCertificate(networkName);
        networkCertificate.setNetworkAdmin(networkAdmin);
        byte[] buffer = new byte[32];
        random.nextBytes(buffer);
        networkCertificate.setNetworkKey(buffer);
        try {
            Enumeration<NetworkInterface> interfaceEnumeration = NetworkInterface.getNetworkInterfaces();
            for(NetworkInterface networkInterface : Collections.list(interfaceEnumeration)) {
                log.debug("Adding " + networkInterface.getDisplayName() + " (" + networkInterface.getDisplayName() + ") to network certificate");
                for(InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses()) {
                    log.debug("Adding " + interfaceAddress.getAddress().getHostAddress() + " to certificate");
                    networkCertificate.getNetworkHosts().add(interfaceAddress.getAddress().getHostAddress());
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return networkCertificate;
    }
}
