package at.femoweb.secnet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by felix on 12/31/14.
 */
public interface Certificate {

    public abstract File store() throws IOException;
    public abstract void load(File file) throws IOException;

    public abstract String getName();
}
