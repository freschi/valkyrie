package at.femoweb.secnet;

import com.sun.xml.internal.bind.v2.runtime.reflect.Lister;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.ByteBuffer;

/**
 * Created by felix on 12/31/14.
 */
public abstract class AbstractPackage implements Package {

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public enum AccessType {
        READ_ONLY, WRITE
    }

    private byte[] bytes;
    private ByteArrayInputStream inputStream;
    private AccessType accessType;

    public AbstractPackage (byte[] content) {
        this.bytes = content;
        this.inputStream = new ByteArrayInputStream(content);
        accessType = AccessType.READ_ONLY;
        try {
            read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public AbstractPackage () {
        accessType = AccessType.WRITE;
    }

    public ByteArrayInputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(ByteArrayInputStream inputStream) {
        this.inputStream = inputStream;
    }

    public AccessType getAccessType() {
        return accessType;
    }

    public void setAccessType(AccessType accessType) {
        this.accessType = accessType;
    }

    public abstract void read() throws IOException;

    public static final Package createPackage(byte[] content) throws IOException {
        int packType = intFromByte(content, 0);
        if(packType == API.BINARY_PACKAGE) {
            return new BinaryPackage(content);
        } else {
            //TODO create package over event based API
            return null;
        }
    }

    public static Package createPackage(String src, String dst, String network) {
        Package pack = new BinaryPackage();
        pack.setSourceName(src);
        pack.setDestinationName(dst);
        pack.setNetworkName(network);
        pack.setApiLevel(API.API_LEVEL);
        return pack;
    }

    public static final byte[] intToByteArray(int number) {
        return ByteBuffer.allocate(4).putInt(number).array();
    }

    public static final int intFromByte(byte[] bytes, int offset) {
        byte[] intBytes = new byte[4];
        System.arraycopy(bytes, offset, intBytes, 0, 4);
        return ByteBuffer.wrap(intBytes).getInt();
    }
}
