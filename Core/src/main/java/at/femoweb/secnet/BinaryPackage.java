package at.femoweb.secnet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;

/**
 * Created by felix on 12/31/14.
 */
public class BinaryPackage extends AbstractPackage {

    /**
     * Declares the end of a field
     */
    public static final char FIELD_END = 0x00;

    /**
     * Declares the beginning of the Destination field (String, end escaped)
     */
    public static final char DESTINATION_FIELD = 0x01;

    /**
     * Declares the beginning of the Source field (String, end escaped)
     */
    public static final char SOURCE_FIELD = 0x02;

    /**
     * Declares the beginning of the API level field (int)
     */
    public static final char API_LEVEL_FIELD = 0x03;

    /**
     * Declares the beginning of the network name field (String, end escaped)
     */
    public static final char NETWORK_NAME_FIELD = 0x04;

    /**
     * Declares the beginning of the content length field (int)
     */
    public static final char CONTENT_LENGTH_INT_FIELD = 0x05;

    /**
     * Declares the beggining of the content
     *
     * If neither of the content length fields is present it is handled as (String, end escaped)
     *
     * If one is present it is handled as (String, length known)
     */
    public static final char CONTENT_FIELD = 0x06;

    private String destinationName;
    private String sourceName;
    private int apiLevel;
    private String networkName;
    private byte[] content;
    private int contentLength;

    public BinaryPackage(byte[] content) {
        super(content);
    }

    public BinaryPackage () {
        super();
    }

    @Override
    public void read() throws IOException {
        InputStream inputStream = getInputStream();
        try {
            inputStream.skip(4);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int val;
        int mode = 0;
        int curr_field = FIELD_END;
        int content_length = -1;
        int content_pos = -1;
        int pos = 4;
        while ((val = inputStream.read()) >= 0) {
            if(mode == 0) {
                if(val == CONTENT_LENGTH_INT_FIELD || val == API_LEVEL_FIELD) {
                    int v = intFromByte(getBytes(), pos + 1);
                    pos += 4;
                    inputStream.skip(4);
                    if(val == CONTENT_LENGTH_INT_FIELD) {
                        contentLength = content_length = v;
                    } else if (val == API_LEVEL_FIELD) {
                        apiLevel = v;
                    }
                } else {
                    if(val == CONTENT_FIELD) {
                        content_pos = pos + 1;
                    }
                    curr_field = val;
                    mode = 1;
                }
            } else if (mode == 1) {
                if(val == FIELD_END) {
                    switch (curr_field) {
                        case DESTINATION_FIELD:
                            destinationName = buffer.toString("UTF-8");
                            break;
                        case SOURCE_FIELD:
                            sourceName = buffer.toString("UTF-8");
                            break;
                        case NETWORK_NAME_FIELD:
                            networkName = buffer.toString("UTF-8");
                            break;
                    }
                    buffer.close();
                    buffer = new ByteArrayOutputStream();
                    mode = 0;
                } else {
                    buffer.write((byte) val);
                }
            }
            pos++;
        }
        if(content_length >= 0 && content_pos >= 0) {
            System.arraycopy(getBytes(), content_pos, content = new byte[content_length], 0, content_length);
        }
    }

    @Override
    public String getDestinationName() {
        return destinationName;
    }

    @Override
    public void setDestinationName(String destinationName) {
        if(getAccessType() == AccessType.WRITE) {
            this.destinationName = destinationName;
        }
    }

    @Override
    public String getSourceName() {
        return sourceName;
    }

    @Override
    public void setSourceName(String sourceName) {
        if(getAccessType() == AccessType.WRITE) {
            this.sourceName = sourceName;
        }
    }

    @Override
    public int getRemoteApiLevel() {
        return apiLevel;
    }

    @Override
    public void setApiLevel(int apiLevel) {
        if(getAccessType() == AccessType.WRITE) {
            this.apiLevel = apiLevel;
        }
    }

    @Override
    public String getNetworkName() {
        return networkName;
    }

    @Override
    public void setNetworkName(String networkName) {
        if(getAccessType() == AccessType.WRITE) {
            this.networkName = networkName;
        }
    }

    @Override
    public byte[] getContent() {
        return content;
    }

    @Override
    public void setContent(byte[] content) {
        if(getAccessType() == AccessType.WRITE) {
            this.content = content;
            this.contentLength = content.length;
        }
    }

    @Override
    public byte[] getEncodedPackage() throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        outputStream.write(intToByteArray(API.BINARY_PACKAGE));
        outputStream.write(SOURCE_FIELD);
        outputStream.write(sourceName.getBytes("UTF-8"));
        outputStream.write(FIELD_END);
        outputStream.write(DESTINATION_FIELD);
        outputStream.write(destinationName.getBytes("UTF-8"));
        outputStream.write(FIELD_END);
        outputStream.write(API_LEVEL_FIELD);
        outputStream.write(intToByteArray(API.API_LEVEL));
        outputStream.write(NETWORK_NAME_FIELD);
        outputStream.write(networkName.getBytes("UTF-8"));
        outputStream.write(FIELD_END);
        outputStream.write(CONTENT_LENGTH_INT_FIELD);
        outputStream.write(intToByteArray(contentLength));
        outputStream.write(CONTENT_FIELD);
        outputStream.write(content);
        return outputStream.toByteArray();
    }
}
