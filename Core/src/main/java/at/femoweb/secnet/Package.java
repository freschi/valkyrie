package at.femoweb.secnet;

import java.io.IOException;

/**
 * Created by felix on 12/31/14.
 */
public interface Package {

    public String getDestinationName();
    public void setDestinationName(String destinationName);

    public String getSourceName();
    public void setSourceName(String sourceName);

    public int getRemoteApiLevel();

    public void setApiLevel(int apiLevel);

    public String getNetworkName();
    public void setNetworkName(String networkName);

    public byte[] getContent();
    public void setContent(byte[] content);

    public byte[] getEncodedPackage() throws IOException;
}
