package at.femoweb.secnet;

import java.io.*;
import java.util.HashMap;
import java.util.Properties;

/**
 * Created by felix on 12/31/14.
 */
public final class CertificateStore {

    private static final HashMap<String, Certificate> certificates = new HashMap<>();
    private static PasswordRequest passwordRequest;

    public static void load() throws IOException {
        File store = new File("certs/store.properties");
        Properties properties = new Properties();
        properties.load(new FileInputStream(store));
        for(String s : properties.stringPropertyNames()) {
            String cert = properties.getProperty(s);
            String clazz = cert.split(";")[0];
            String file = cert.split(";")[1];
            try {
                Certificate certificate = (Certificate) Class.forName(clazz).newInstance();
                certificate.load(new File(file));
                certificates.put(s, certificate);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public static void store() throws IOException {
        File store = new File("certs/store.properties");
        Properties properties = new Properties();
        for(String s : certificates.keySet()) {
            Certificate certificate = certificates.get(s);
            File file = certificate.store();
            String info = certificate.getClass().getName() + ";" + file.getPath();
            properties.setProperty(s, info);
        }
        properties.store(new FileOutputStream(store), "Certificate Store info");
    }

    public static void add(Certificate certificate) {
        certificates.put(certificate.getName(), certificate);
    }

    public static Certificate get(String name) {
        return certificates.get(name);
    }

    public static void remove(String name) {
        certificates.remove(name);
    }

    public static void remove(Certificate certificate) {
        remove(certificate.getName());
    }

    public static boolean has(String name) {
        return certificates.containsKey(name);
    }

    public static boolean hasAnyPrefix(String name) {
        return certificates.keySet().stream().anyMatch(s -> s.startsWith(name));
    }

    public static String requestPassword(String request) {
        if(passwordRequest != null)
            return passwordRequest.requestPassword(request);
        return null;
    }

    public static PasswordRequest getPasswordRequest() {
        return passwordRequest;
    }

    public static void setPasswordRequest(PasswordRequest passwordRequest) {
        CertificateStore.passwordRequest = passwordRequest;
    }
}

