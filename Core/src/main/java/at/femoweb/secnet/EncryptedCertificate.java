package at.femoweb.secnet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

/**
 * Created by felix on 12/31/14.
 */
public abstract class EncryptedCertificate implements Certificate {

    private static final Logger log = LogManager.getLogger("Secnet-Core");

    public abstract byte[] getEncryptedContent ();
    public abstract void loadFromEncrypted(byte[] content);

    public abstract File getCertificateFile();

    @Override
    public File store() throws IOException {
        File file = getCertificateFile();
        if(file.exists()) {
            log.info("Certificate file " + file.getAbsolutePath() + " already exists. Overwriting it!");
        }
        if(!file.getParentFile().exists()) {
            log.debug("Creating parent directory for " + getName());
            file.getParentFile().mkdirs();
        }
        byte[] content = getEncryptedContent();
        log.debug("Certificate " + getName() + " has a length of " + content.length);
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(content);
        fileOutputStream.close();
        log.debug("Written Certificate " + getName() + " to " + file.getAbsolutePath());
        return file;
    }

    @Override
    public void load(File file) throws IOException {
        byte[] buff = new byte[(int) file.length()];
        log.debug("Loading " + getName() + " from " + file.getAbsolutePath() + " with length " + file.length());
        FileInputStream fileInputStream = new FileInputStream(file);
        fileInputStream.read(buff, 0, buff.length);
        fileInputStream.close();
        log.debug("Loading completed");
        loadFromEncrypted(buff);
        log.info("Certificate successfully loaded " + getName());
    }
}
