package at.femoweb.secnet;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Properties;

/**
 * Created by felix on 12/31/14.
 */
public class NetworkCertificate extends EncryptedCertificate {

    private String networkName;
    private byte[] networkKey;
    private List<String> networkHosts;
    private String networkAdmin;

    public NetworkCertificate () {};

    public NetworkCertificate(String networkName) {
        this.networkName = networkName;
        this.networkHosts = new ArrayList<>();
    }

    @Override
    public byte[] getEncryptedContent() {
        Properties properties = new Properties();
        properties.setProperty("network.name", networkName);
        properties.setProperty("network.admin", networkAdmin);
        properties.setProperty("network.key", Base64.getEncoder().encodeToString(networkKey));
        properties.setProperty("network.hosts.length", networkHosts.size() + "");
        for (int i = 0; i < networkHosts.size(); i++) {
            properties.setProperty("network.hosts." + i, networkHosts.get(i));
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            properties.store(outputStream, "Network Certificate. If you can read this shred this file immediately and leave no trace of it anywhere!");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String key = CertificateStore.requestPassword("Network Key for " + networkName);
        byte[] data = outputStream.toByteArray();
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] secret = digest.digest(key.getBytes("UTF-8"));
            SecretKeySpec secretKeySpec = new SecretKeySpec(secret, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            data = cipher.doFinal(data);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return data;
    }

    @Override
    public void loadFromEncrypted(byte[] content) {
        String key = CertificateStore.requestPassword("Network Key for " + networkName);
        byte[] data = content;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] secret = digest.digest(key.getBytes("UTF-8"));
            SecretKeySpec secretKeySpec = new SecretKeySpec(secret, "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            data = cipher.doFinal(data);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        networkName = properties.getProperty("network.name");
        networkAdmin = properties.getProperty("network.admin");
        networkKey = Base64.getDecoder().decode(properties.getProperty("network.key"));
        int hostsLength = Integer.parseInt(properties.getProperty("network.hosts.length"));
        networkHosts = new ArrayList<>();
        for (int i = 0; i < hostsLength; i++) {
            networkHosts.add(properties.getProperty("network.hosts." + i));
        }
    }

    @Override
    public File getCertificateFile() {
        return new File("certs/networks/net-" + networkName + ".cert");
    }

    @Override
    public String getName() {
        return "net-" + networkName;
    }

    public byte[] getNetworkKey() {
        return networkKey;
    }

    public void setNetworkKey(byte[] networkKey) {
        this.networkKey = networkKey;
    }

    public List<String> getNetworkHosts() {
        return networkHosts;
    }

    public void setNetworkHosts(List<String> networkHosts) {
        this.networkHosts = networkHosts;
    }

    public String getNetworkAdmin() {
        return networkAdmin;
    }

    public void setNetworkAdmin(String networkAdmin) {
        this.networkAdmin = networkAdmin;
    }

    public String getNetworkName() {
        return networkName;
    }

    public void setNetworkName(String networkName) {
        this.networkName = networkName;
    }
}
