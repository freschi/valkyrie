package at.femoweb.secnet;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class BinaryPackageTest {

    @Test
    public void testGetContent() throws Exception {
        Package pack = new BinaryPackage();
        pack.setApiLevel(API.API_LEVEL);
        pack.setNetworkName("Testnet");
        pack.setDestinationName("felix.resch");
        pack.setSourceName("__keymaster");
        pack.setContent("Hello World".getBytes());
        byte[] encodedPackage = pack.getEncodedPackage();
        for (int i = 0; i < encodedPackage.length; i++) {
            System.out.printf("%03d ", encodedPackage[i] < 0 ? 0xff + encodedPackage[i] : encodedPackage[i]);
        }
        System.out.println();
        for (int i = 0; i < encodedPackage.length; i++) {
            System.out.printf("%3c ", encodedPackage[i] < 0 ? 0xff + encodedPackage[i] : encodedPackage[i]);
        }
        System.out.println();
        Package rcvd = AbstractPackage.createPackage(encodedPackage);
        System.out.println(rcvd.getSourceName() + " -> " + rcvd.getDestinationName() + ": " + new String(rcvd.getContent(), "UTF-8"));
        System.out.println("\tRemote API Level: " + rcvd.getRemoteApiLevel());
        System.out.println("\tNetwork Name: " + rcvd.getNetworkName());
        Assert.assertArrayEquals(pack.getContent(), rcvd.getContent());
        Assert.assertEquals(pack.getSourceName(), rcvd.getSourceName());
        Assert.assertEquals(pack.getDestinationName(), rcvd.getDestinationName());
        Assert.assertEquals(pack.getNetworkName(), rcvd.getNetworkName());
        Assert.assertEquals(pack.getRemoteApiLevel(), rcvd.getRemoteApiLevel());
        Assert.assertArrayEquals(encodedPackage, rcvd.getEncodedPackage());
    }

    @Test
    public void testCreatePackage() throws Exception {
        Package pack = AbstractPackage.createPackage("__keymaster", "felix.resch", "Testnet");
        Assert.assertEquals("__keymaster", pack.getSourceName());
        Assert.assertEquals("felix.resch", pack.getDestinationName());
        Assert.assertEquals("Testnet", pack.getNetworkName());
        Assert.assertEquals(API.API_LEVEL, pack.getRemoteApiLevel());
        pack.setContent("Hello, how are you doing?".getBytes());
        byte[] encodedPackage = pack.getEncodedPackage();
        for (int i = 0; i < encodedPackage.length; i++) {
            System.out.printf("%03d ", encodedPackage[i] < 0 ? 0xff + encodedPackage[i] : encodedPackage[i]);
        }
        System.out.println();
        for (int i = 0; i < encodedPackage.length; i++) {
            System.out.printf("%3c ", encodedPackage[i] < 0 ? 0xff + encodedPackage[i] : encodedPackage[i]);
        }
        System.out.println();
        System.out.println();
    }
}